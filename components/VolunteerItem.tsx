interface ContainerProps { }

const VolunteerItem = (props) => {
  
  const divStyle = {    
    backgroundImage: 'url(' + props['image'] + ')',
  };
  
  return (
    <div className="mt-5 flex justify-start">
      <div>
        <div className="bg-gray-200 rounded-md h-48 w-48 bg-cover" style={divStyle}>
        </div>
      </div>
      <div className="ml-5">
        <h1 className="text-xl font-bold">{props.role}</h1>
        <h1 className="text-xl font-bold">{props['title']}</h1>
        <h2 className="italic">{props['period']}</h2>
        <p className="mt-3">
          {props.children}
        </p>
      </div> 
  
  </div>
  
  );
};

export default VolunteerItem;
