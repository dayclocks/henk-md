import React from 'react';
/**
 * Cannot find module '../content/volunteer.md' or its corresponding type declarations.ts(2307)
 */
import { react as Volunteer} from '../content/volunteer.md';

const VolunteerExperience = () => {
  console.log(Volunteer);

  return (

<div className="py-12 bg-white">
  <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
    <div className="lg:text-center">
      <p className="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
        Volunteer Experience
      </p>
      <p className="mt-5 text-lg ">
        <Volunteer></Volunteer>
      </p>
    </div>

  </div>
</div>


  );
};

export default VolunteerExperience;
