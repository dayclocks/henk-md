interface ContainerProps { }

const About: React.FC<ContainerProps> = () => {
  return (

<div className="py-12 bg-white">
  <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
    <div className="lg:text-center">
      <p className="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
          About me
      </p>
    </div>
  </div>

  <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">

    <div className="mt-10 grid grid-cols-3 gap-10">
      <div className="col-span-2">
        <p>
          Hi, I have a broad experience as a software developer in various industries, including the national government, financial and public sector.
        </p>

        <p className="mt-4">I'm pragmatic, team player, proactive, solution-oriented in complex environments where quality remains important. 
          Preferably as part of a team in a scrum / agile environment, close to the customer.</p>

        <p className="mt-4">Realizing solutions that provide added value for the customer is my main motivation. I prefer technically challenging projects / assignments, but I also still find it satisfying in assignments where proven technology is used.
        </p>
      </div>

      <div className="grid justify-items-center">
        <div className="rounded-full h-32 w-32 flex items-center justify-center bg-red-500 bg-cover" style={{ backgroundImage : 'url(henk.jpg)'}} >

        </div>
      </div>
      
    </div>  


  </div>  






</div>


  );
};

export default About;
