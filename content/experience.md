---
title: experience
date: 2019-03-17T19:31:20.591Z
experience:
  - title: Trivento
    image: trivento.png
    period: january 2021 - current

  - title: Belastingdienst 
    image: belastingdienst.png
    period: juni 2020 - december 2020

  - title: Rabobank
    image: rabobank.png
    period: february 2018 - february 2020

  - title: CRV
    image: crv.png
    period: november 2015 - augustus 2017

---
