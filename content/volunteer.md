---
title: experience
date: 2019-03-17T19:31:20.591Z
experience:
  - role: Teacher ITC & Technics
    title: Stad & Esch
    image: trivento.png
    period: january 2015 - april 2016
    description: Teaching students in the age of 13- 14 coding. The students learning how to create a website, a real mobile app, coding in Python, building 3D a printer and a lot more!

  - role: Teacher
    title: Devoxx4Kids
    image: belastingdienst.png
    period: june 2015 - june 2015
    description: In june I had the opportunity from Devoxx4Kids to give workshop in Boston. Red Hat organises every year the Red Hat Summit. This year Red Hat also hosted for the first time Devoxx4Kids. Devoxx4Kids is an organization to introduce teenagers to programming, robotics and engineering in a fun way. The title of my workshop was Build your own Flappy Bird, to get teens acquainted with HTML5, CSS and Javascript with as result a Flappy Bird game!

---
